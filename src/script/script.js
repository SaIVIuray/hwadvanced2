function handleScroll() {
    const windowWidth = window.innerWidth;
    const navbar = document.querySelector('.navbar-content');

    if (windowWidth >= 1200) {
        const navbarOffset = navbar.offsetTop;

        if (window.pageYOffset >= navbarOffset) {
            navbar.classList.add('fixed');
        } else {
            navbar.classList.remove('fixed');
        }

        if (window.pageYOffset === 0) {
            navbar.classList.remove('fixed');
        }
    } else {
        navbar.classList.remove('fixed');
    }
}

function handleResize() {
    const windowWidth = window.innerWidth;
    const navbar = document.querySelector('.navbar-content');

    if (windowWidth < 1200) {
        navbar.classList.remove('fixed');
    }
}

window.addEventListener('scroll', handleScroll);
window.addEventListener('resize', handleResize);